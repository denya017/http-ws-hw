const username = sessionStorage.getItem("username");
const createRoomButton = document.getElementById("add-room-btn");
const quitRoomButton = document.getElementById('quit-room-btn');

if (!username) {
  window.location.replace("/login");
}

let letterCounter = -1;
const socket = io("", { query: { username } });
let ready;


socket.on('authorisation', (arg) => {
  if (!arg) {
    alert('This username is taken!');
    sessionStorage.removeItem("username");
    window.location.replace("/login");
  }
});

socket.on('roomCheck', (arg) => {
  if (!arg) {
    alert('This name of the room is taken!');
  }
});

socket.on('showRooms', (rooms) => {
  document.getElementById('rooms').innerHTML = '';
  for (const roomName in rooms) {
    createRoom(roomName, Object.keys(rooms[roomName]).length);
  }
});

const onClickCreateRoomButton = () => {
  const nameRoom = window.prompt('Enter the name of the room');
  if (!nameRoom || nameRoom === '') {
    return;
  }
  socket.emit('newRoom', nameRoom);
  joinRoom(nameRoom);
};

socket.on('newRoom', (room) => {
  createRoom(room);
});

socket.on('showGamers', (gamers) => showGamers(gamers));

socket.on('startGame', (text) => startGame(text));

function createRoom(roomName, userCount) {
  const roomCard = document.createElement('div');
  roomCard.className = 'roomCard padding';
  const users = document.createElement('p');
  users.innerHTML = userCount + ' user connected';
  roomCard.append(users);
  const name = document.createElement('h2');
  name.innerHTML = roomName;
  roomCard.append(name);
  const btn = document.createElement('button');
  roomCard.append(btn);
  btn.innerHTML = 'Join';
  btn.className = 'join-btn btn';
  btn.addEventListener('click', () => joinRoom(roomName));
  document.getElementById('rooms').append(roomCard);
}

function joinRoom(roomName) {
  socket.emit('joinRoom', roomName);
  document.getElementById('rooms-page').className += ' display-none';
  document.getElementById('game-page').className = 'full-screen flex padding';
  document.getElementById('roomName').innerHTML = roomName;
  ready = false;
  readyCheck();
}

function leaveRoom() {
  socket.emit('leaveRoom');
  document.getElementById('rooms-page').className = 'full-screen padding';
  document.getElementById('game-page').className += ' display-none';
  removeEventListener('keydown', pressed);
  document.getElementById('answer').innerHTML = '';
}

function showGamers(users) {
  document.getElementById('gamers').innerHTML = '';
  for (const user of users) {
    const gamer = document.createElement('div');
    gamer.id = user.userId;
    gamer.className = 'padding';
    const ready = document.createElement('span');
    if (user.status) {
      ready.className = 'ready-status-green';
    }
    else {
      ready.className = 'ready-status-red';
    }
    gamer.append(ready);
    const gamerName = document.createElement('span');
    gamerName.innerHTML = user.username;
    if (user.username === username)
      gamerName.innerHTML += ' (you)';
    gamer.append(gamerName);
    const progress = document.createElement('div');
    gamer.append(progress);
    const progressBar = document.createElement('progress');
    progressBar.max = 100;
    if (user.progress > 0)
      progressBar.value = user.progress;
    progressBar.className = `user-progress ${user.userId}`;
    progress.append(progressBar);
    document.getElementById('gamers').append(gamer);
  }
}

const pressed = function (event) {
  checkKey(event.key, document.getElementById('text-container').innerHTML);
};

function readyCheck() {
  document.getElementById('game-zone').innerHTML = `<button id="ready-btn" class="btn">Ready</button>`;
  const readyBtn = document.getElementById('ready-btn');
  readyBtn.addEventListener('click', ()=>{
    if (!ready) {
      readyBtn.innerHTML = 'Not ready';
      ready = true;
      document.getElementById(socket.id).querySelector('span').className = 'ready-status-green';
    }
    else {
      readyBtn.innerHTML = 'Ready';
      ready = false;
      document.getElementById(socket.id).querySelector('span').className = 'ready-status-red';
    }
    socket.emit('readyCheck', ready);
  });
}

function startGame(text) {
  const textContainer = document.createElement('div');
  document.getElementById('game-zone').innerHTML ='';
  textContainer.id = 'text-container';
  textContainer.innerHTML = text;
  document.getElementById('game-zone').append(textContainer);
  letterCounter = 0;
  addEventListener('keydown', pressed);
}

function checkKey(key, text) {
  console.log('k' +key);
  console.log('l'+Array.from(text)[letterCounter]);
  if (key === Array.from(text)[letterCounter]) {
    console.log('right');
    letterCounter++;
    socket.emit('updateProgressBar', (letterCounter*100/Array.from(text).length));
    document.getElementById('answer').innerHTML+=key;
  }
}

createRoomButton.addEventListener("click", onClickCreateRoomButton);
quitRoomButton.addEventListener('click', leaveRoom);