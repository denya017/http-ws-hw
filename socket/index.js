import * as config from "./config";
import { texts } from "../data";

export default io => {
  //user: userId
  const users = {};
  //room: [{user, userId, status, progress}, ...]
  const rooms = {};

  io.on("connection", socket => {
    const username = socket.handshake.query.username;
    let currentRoom = '';

    if (users.hasOwnProperty(username)) {
        socket.emit('authorisation', false);
    }
    else {
        users[username] = '';
    }

    socket.emit('showRooms', rooms);

    socket.on('newRoom', (roomName) => {
        if (rooms.hasOwnProperty(roomName)) {
            socket.emit('roomCheck', false);
            return;
        }
        rooms[roomName] = [];
        io.emit('showRooms', rooms);
        showUsersInRoom(roomName);
    });

    socket.on('joinRoom', (roomName) => {
        socket.join(roomName);
        currentRoom = roomName;
        users[username] = roomName;
        rooms[roomName].push({
            username,
            userId: socket.id,
            status: false,
            progress: 0
        });
        io.emit('showRooms', rooms);
        showUsersInRoom(roomName);
    });

    socket.on('leaveRoom', () => leaveRoom());

    socket.on('updateProgressBar', (value) => {
        for (const user of rooms[currentRoom]) {
            if (user.username === username) {
                user.progress = value;
            }
        }
        showUsersInRoom(currentRoom);
    });

    socket.on('disconnect', () => {
        if(currentRoom !== '') {
            leaveRoom();
        }
        delete users[username];
    });

    socket.on('readyCheck', (boolCheck) => {
        let counter = 0;
        for (const user of rooms[currentRoom]) {
            if (user.username === username) {
                user.status = boolCheck;
                socket.to(currentRoom).emit('showGamers', rooms[currentRoom]);
            }
            if (user.status) {
                counter++;
            }
        }
        if (counter === rooms[currentRoom].length) {
            io.emit('startGame', texts[0]);
        }
    })

    function leaveRoom() {
        socket.leave(currentRoom);
        const room = currentRoom;
        users[username] = '';
        for (const user of rooms[currentRoom]) {
            if (user.username === username) {
                rooms[currentRoom].splice(rooms[currentRoom].indexOf(user), 1);
            }
        }
        if (Object.keys(rooms[currentRoom]).length === 0) {
            delete rooms[currentRoom];
        }
        currentRoom = '';
        io.emit('showRooms', rooms);
        showUsersInRoom(room);
    }

    function showUsersInRoom(roomName) {
        io.to(roomName).emit('showGamers', rooms[roomName]);
    }
  });
};
